//
//  CacheManagerTest.swift
//  WebDownloadCacheTests
//
//  Created by Faraz Khan on 7/1/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import XCTest
@testable import WebDownloadCache
class CacheManagerTest: XCTestCase {
    
    var cacheManager:CacheManagerProtocol!
    let testJSONKey = "testJSONKey"
    let testImageKey = "testImageKey"
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        cacheManager = CacheManager()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testQueryImageCache_NoImageForKey() {
        let expectation = XCTestExpectation.init(description: "testQueryImageCache_NoImageForKey");
        cacheManager.queryImageCache(forKey: testImageKey) { (image, error) in
            XCTAssert(image == nil)
            expectation.fulfill()
        }
    }
    
    
    func testQueryJSONCache_NoJSONForKey() {
        let expectation = XCTestExpectation.init(description: "testQueryJSONCache_NoJSONForKey");
        cacheManager.queryJSONCache(forKey: testJSONKey) { (data, error) in
            XCTAssert(data == nil)
            expectation.fulfill()
        }
    }
    
    func testQueryJSONCache_WithValidJSONKey() {
        let expectation = XCTestExpectation.init(description: "testQueryImageCache_WithValidJSONKey");
        let mockJsonData = Data()
        cacheManager.store(mockJsonData, forKey: testJSONKey)
        cacheManager.queryJSONCache(forKey: testJSONKey) { (data, error) in
            XCTAssert(data != nil)
            XCTAssert(data == mockJsonData)
            expectation.fulfill()
        }
    }
    
    func testQueryImageCache_WithValidImageKey() {
        let expectation = XCTestExpectation.init(description: "testQueryImageCache_WithValidJSONKey");
        let mockImage = UIImage()
        cacheManager.store(mockImage, imageData: Data(), forKey: testImageKey)
        cacheManager.queryImageCache(forKey: testImageKey) { (image, error) in
            XCTAssert(image != nil)
            XCTAssert(image == mockImage)
            expectation.fulfill()
        }
    }
    
    func testClearCache() {
        let expectation = XCTestExpectation.init(description: "testQueryImageCache_WithValidJSONKey");
        let mockImage = UIImage()
        cacheManager.store(mockImage, imageData: Data(), forKey: testImageKey)
        cacheManager.queryImageCache(forKey: testImageKey) { (image, error) in
            XCTAssert(image != nil)
            XCTAssert(image == mockImage)
            expectation.fulfill()
        }
        
        let mockJsonData = Data()
        cacheManager.store(mockJsonData, forKey: testJSONKey)
        cacheManager.queryJSONCache(forKey: testJSONKey) { (data, error) in
            XCTAssert(data != nil)
            XCTAssert(data == mockJsonData)
            expectation.fulfill()
            expectation.fulfill()
        }
        
        cacheManager.clearMemory()
        
        cacheManager.queryImageCache(forKey: testImageKey) { (image, error) in
            XCTAssert(image == nil)
            expectation.fulfill()
        }
        
        cacheManager.queryJSONCache(forKey: testJSONKey) { (data, error) in
            XCTAssert(data == nil)
            expectation.fulfill()
        }
        
        
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
