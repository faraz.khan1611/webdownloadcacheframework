//
//  downloader.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/25/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit
protocol downloaderTypeAlias {
    typealias downloadCompletedCallback = (Data?, Error?, Bool) -> Void
}
class Downloader: NSObject,URLSessionDataDelegate,URLSessionTaskDelegate,downloaderTypeAlias,DownloaderProtocol {
    private var downloaderQueue: OperationQueue!
    private var urlOperations: [URL : DownloadOperation] = [:]
    private var httpHeaders = [String:String]()
    private var operationsLock: DispatchSemaphore!
    private var headersLock: DispatchSemaphore!
    private var session: URLSession?
    private var downloadTimeout: TimeInterval
    
    var allHTTPHeaders: [String : String]? {
        headersLock.wait()
        let allHTTPHeaderFields = httpHeaders
        headersLock.signal()
        return allHTTPHeaderFields
    }
    
    convenience override init() {
        self.init(sessionConfiguration: URLSessionConfiguration.default)
    }
    
    init(sessionConfiguration: URLSessionConfiguration?) {
        downloaderQueue = OperationQueue()
        downloaderQueue.maxConcurrentOperationCount = 6
        downloaderQueue.name = "com.webdownloadCache.downloaderQueue"
        operationsLock = DispatchSemaphore(value: 1)
        headersLock = DispatchSemaphore(value: 1)
        downloadTimeout = 15.0
        super.init()
        createSession(with: sessionConfiguration)
    }
    
    func createSession(with sessionConfiguration: URLSessionConfiguration?) {
        cancelAllDownloads()
        
        if session != nil {
            session?.invalidateAndCancel()
        }
        
        sessionConfiguration?.timeoutIntervalForRequest = downloadTimeout
        if let sessionConfiguration = sessionConfiguration {
            session = URLSession(configuration: sessionConfiguration, delegate: self, delegateQueue: nil)
        }
    }
    
    func invalidateSessionAndCancel(_ cancelPendingOperations: Bool) {
        if cancelPendingOperations {
            session?.invalidateAndCancel()
        } else {
            session?.finishTasksAndInvalidate()
        }
    }
    
    deinit {
        session?.invalidateAndCancel()
        session = nil
        
        downloaderQueue.cancelAllOperations()
    }
    
    func value(forHTTPHeaders field: String) -> String? {
        if field == "" {
            return nil
        }
        return allHTTPHeaders?[field]
    }
    
    
    func setValue(_ value: String?, forHTTPHeaders field: String) {
        headersLock.wait()
        if value != nil {
            httpHeaders[field] = value
        } else {
            httpHeaders.removeValue(forKey: field)
        }
        headersLock.signal()
    }
    
    func setMaxConcurrentDownloads(_ maxConcurrentDownloads: Int) {
        downloaderQueue.maxConcurrentOperationCount = maxConcurrentDownloads
    }
    
    func currentDownloadCount() -> Int {
        return downloaderQueue.operationCount
    }
    
    func maxConcurrentDownloads() -> Int {
        return downloaderQueue.maxConcurrentOperationCount
    }
    
    func sessionConfiguration() -> URLSessionConfiguration? {
        return session?.configuration
    }
    
    func cancelAllDownloads() {
        downloaderQueue.cancelAllOperations()
    }
    
    func cancel(_ token: DownloadToken?) {
        let url = token?.url
        if url == nil {
            return
        }
        operationsLock.wait()
        var operation: DownloadOperation? = nil
        operation = urlOperations[url!]
        if operation != nil {
            let canceled = operation?.cancel(token?.cancelToken)
            if canceled ?? false {
                urlOperations.removeValue(forKey:url!)
            }
        }
        operationsLock.signal()
    }
    
    func downloadData(with url: URL?, isURLCachingEnabled:Bool, completed completedBlock: downloadCompletedCallback?) -> DownloadToken? {
        weak var weakSelf = self
        
        return addCallbackAndReturnOperation(_completedBlock: completedBlock, for: url, createCallback: {() in
            let strongSelf = weakSelf
            var timeoutInterval = strongSelf?.downloadTimeout
            if timeoutInterval == 0.0 {
                timeoutInterval = 15.0
            }
            
            
            let cachePolicy: NSURLRequest.CachePolicy = isURLCachingEnabled ? .useProtocolCachePolicy : .reloadIgnoringLocalCacheData
            var request: URLRequest? = nil
            if let url = url {
                request = URLRequest.init(url: url, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval ?? 0.0)
            }
            let operation = DownloadOperation.init(request: request, in: strongSelf?.session)
            request?.allHTTPHeaderFields = strongSelf?.allHTTPHeaders
            return operation
        })
    }
    
    func addCallbackAndReturnOperation(_completedBlock: downloadCompletedCallback?, for url: URL?, createCallback: @escaping () -> DownloadOperation) -> DownloadToken? {
        if url == nil {
            if let block = _completedBlock {
                block(nil, nil, false)
            }
            return nil
        }
        
        operationsLock.wait()
        var operation = urlOperations[url!]
        if operation == nil {
            operation = createCallback()
            weak var wself = self
            operation?.completionBlock = {
                let sself = wself
                if sself == nil {
                    return
                }
                sself?.operationsLock.wait()
                sself?.urlOperations.removeValue(forKey: url!)
                sself?.operationsLock.signal()
            }
            urlOperations[url!] = operation
            if let operate = operation {
                downloaderQueue.addOperation(operate)
            }
        }
        operationsLock.signal()
        let cancelToken = operation!.addHandlers(completed: _completedBlock)
        
        let token = DownloadToken()
        token.downloadOperation = operation
        token.url = url
        token.cancelToken = cancelToken
        return token;
    }
    
    func operation(with task: URLSessionTask?) -> DownloadOperation? {
        var returnOperation: DownloadOperation? = nil
        for case let operation_ as DownloadOperation in downloaderQueue.operations {
            if operation_.dataTask?.taskIdentifier == task?.taskIdentifier {
                returnOperation = operation_
                break
            }
        }
        return returnOperation
    }
    
    //URLSESSION DELEGATE
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        let dataOperation = operation(with: dataTask)
        if dataOperation?.responds(to: #selector(urlSession(_:dataTask:didReceive:))) ?? false {
            dataOperation?.urlSession(session, dataTask: dataTask, didReceive: data)
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        let dataOperation = operation(with: dataTask)
        if dataOperation?.responds(to: #selector(urlSession(_:dataTask:didReceive:completionHandler:))) ?? false {
            dataOperation?.urlSession(session, dataTask: dataTask, didReceive: response, completionHandler: completionHandler)
        } else {
            completionHandler(.allow)
        }
        
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        
        let dataOperation = operation(with: task)
        if dataOperation?.responds(to: #selector(urlSession(_:task:didCompleteWithError:))) ?? false {
            dataOperation?.urlSession(session, task: task, didCompleteWithError: error)
        }
        
    }
    
}
