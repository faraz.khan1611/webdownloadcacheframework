//
//  downloadToken.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/28/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import Foundation

class DownloadToken
{
    var url:URL?
    var cancelToken:Any?
    var downloadOperation:DownloadOperation?;
}
