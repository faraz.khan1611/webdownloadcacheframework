//
//  downloaderProtocol.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/26/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

protocol DownloaderProtocol:downloaderTypeAlias {
    func downloadData(with url: URL?, isURLCachingEnabled:Bool, completed completedBlock: downloadCompletedCallback?) -> DownloadToken?
    func cancel(_ token: DownloadToken?)
    func cancelAllDownloads()
}
