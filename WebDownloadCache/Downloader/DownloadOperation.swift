//
//  downloadOperation.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/25/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

class DownloadOperation: Operation,URLSessionDataDelegate,URLSessionTaskDelegate,downloaderTypeAlias {
    
    private var request: URLRequest?
    var dataTask: URLSessionTask?
    private var expectedSize = 0
    private var response: URLResponse?
    private var callbackBlocks: [downloadCompletedCallback] = []
    private var downloadedData: Data?
    private weak var unownedSession: URLSession?
    private var ownedSession: URLSession?
    private var callbacksLock: DispatchSemaphore!
    private var _executing = false {
        willSet {
            willChangeValue(forKey: "isExecuting")
        }
        didSet {
            didChangeValue(forKey: "isExecuting")
        }
    }
    
    override var isExecuting: Bool {
        return _executing
    }
    
    private var _finished = false {
        willSet {
            willChangeValue(forKey: "isFinished")
        }
        
        didSet {
            didChangeValue(forKey: "isFinished")
        }
    }
    
    override var isFinished: Bool {
        return _finished
    }
    
    func executing(_ executing: Bool) {
        _executing = executing
    }
    
    func finish(_ finished: Bool) {
        _finished = finished
    }
    
    init(request: URLRequest?, in session: URLSession?) {
        super.init()
        self.request = request
        callbackBlocks = []
        expectedSize = 0
        unownedSession = session
        callbacksLock = DispatchSemaphore(value: 1)
    }
    
    override func start() {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            if isCancelled {
                finish(true)
                reset()
                return
            }
            
            var session = unownedSession
            if session == nil {
                let sessionConfig = URLSessionConfiguration.default
                sessionConfig.timeoutIntervalForRequest = 15
                session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
                ownedSession = session
            }
            
            dataTask = session?.dataTask(with: request!)
            executing(true)
        }
        
        if (dataTask != nil) {
            dataTask!.resume()
        } else {
            callCompletionBlocksWithError(error: NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: [
                NSLocalizedDescriptionKey: "Task can't be initialized"
                ]))
            done()
            return
        }
    }
    
    func callCompletionBlocksWithError(error:Error?)
    {
        callCompletionBlocks(with: nil,error: error, finished: false)
    }
    func callCompletionBlocks(with data: Data?, error: Error?, finished: Bool) {
        for completedBlock in callbackBlocks {
            completedBlock(data, error, finished)
        }
    }
    
    func addHandlers(completed completedBlock: downloadCompletedCallback?) -> Any? {
        callbacksLock.wait()
        callbackBlocks.append(completedBlock!)
        callbacksLock.signal()
        return callbacks
    }

    
    func callbacks() -> [downloadCompletedCallback]? {
        return callbackBlocks.filter({ ($0) as AnyObject !== (NSNull()) as AnyObject })
    }

    
    func cancel(_ token: Any?) -> Bool {
        var shouldCancel = false
        callbacksLock.wait()
        callbackBlocks = callbackBlocks.filter({ ($0) as AnyObject !== (token) as AnyObject })
        if callbackBlocks.count == 0 {
            shouldCancel = true
        }
        callbacksLock.signal()
        if shouldCancel {
            cancel()
        }
        return shouldCancel
    }
    
    override func cancel() {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            cancelInternal()
        }
    }
    
    func cancelInternal() {
        if isFinished {
            return
        }
        super.cancel()
        
        if (dataTask != nil) {
            dataTask!.cancel()
        }
        reset()
    }

    
    func done (){
       finish(true)
        executing(false)
        reset()
    }
    
    func reset() {
        callbacksLock.wait()
        callbackBlocks.removeAll()
        callbacksLock.signal()
        dataTask = nil
        
        if (ownedSession != nil) {
            ownedSession?.invalidateAndCancel()
            ownedSession = nil
        }
    }
    
    func isConcurrent() -> Bool {
        return true
    }
    
    //URLSESSION DELEGATE
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        if downloadedData == nil {
            downloadedData = Data(capacity: expectedSize)
        }
        downloadedData?.append(data)
        
        if expectedSize > 0 {
            var tempData = data
            let totalSize = tempData.count
            let finished = totalSize >= expectedSize
            if(finished)
            {
                callCompletionBlocks(with: data, error: nil, finished: finished)
            }
        }
    }
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive response: URLResponse, completionHandler: @escaping (URLSession.ResponseDisposition) -> Void) {
        var disposition: URLSession.ResponseDisposition = .allow
        var expected = Int(response.expectedContentLength)
        expected = expected > 0 ? expected : 0
        expectedSize = expected
        
        let status = (response as? HTTPURLResponse)?.statusCode
        var valid = (status ?? 0) < 400
        //'304 Not Modified' is an exceptional one. It should be treated as cancelled if no cache data
        //URLSession current behavior will return 200 status code when the server respond 304 and URLCache hit. But this is not a standard behavior and we just add a check
        if status == 304 {
            valid = false
        }
        
        if !valid {
            disposition = .cancel;
        }
        completionHandler(disposition)

    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            callCompletionBlocksWithError(error: error)
            done()
        } else {
          done()
        }
    }
}
