//
//  CacheManager.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/26/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

protocol cacheTypeAlias {
    typealias cacheOperationCompletedForImage = (UIImage?, Error?) -> Void
    typealias cacheOperationCompletedForJSONData = (Data?, Error?) -> Void
}

class CacheManager: NSObject,cacheTypeAlias,CacheManagerProtocol {
    
    var memCache: Cache<NSString,CacheObject>?
    var config:cacheConfig!
    
    override init() {
        memCache = Cache<NSString,CacheObject>();
        config = cacheConfig();
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(deleteOldFiles), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(backgroundDeleteCache), name: UIApplication.didEnterBackgroundNotification, object: nil)

    }
    
    init(config_:cacheConfig) {
        memCache = Cache<NSString,CacheObject>();
        config = config_;
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(deleteOldFiles), name: UIApplication.willTerminateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(backgroundDeleteCache), name: UIApplication.didEnterBackgroundNotification, object: nil)
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //FOR IMAGE CACHE
    func queryImageCache(forKey key: String?, done doneBlock: cacheOperationCompletedForImage?) {
        if key == nil {
            if doneBlock != nil {
                doneBlock?(nil, nil)
            }
        }
        if doneBlock != nil {
            let image = imageFromMemoryCache(forKey: key)
            doneBlock!(image,nil);
        }
    }
    
    private func imageFromMemoryCache(forKey key: String?) -> UIImage? {
        let cachedObject = memCache?.getObject(forKey: key! as NSString);
        if cachedObject?.data is UIImage
        {
            return (cachedObject?.data as! UIImage)
        }
        return nil;
    }
    
    func store(_ image: UIImage?, imageData: Data?, forKey key: String?) {
        if image == nil || key == nil {
            return
        }
        // if memory cache is enabled
        if config.shouldCacheImagesInMemory {
            if let image = image {
                let size = imageData?.count ?? 0;
                let cacheObj = CacheObject()
                cacheObj.data = image
                cacheObj.dataSize = size
                cacheObj.key = key!
                NSLog("from CacheManager store image %@",key ?? "")
                memCache?.setObject(cacheObj, forKey: key! as NSString, cost: size)
            }
        }
    }
    
    //For JSON
    func queryJSONCache(forKey key: String?, done doneBlock: cacheOperationCompletedForJSONData?) {
        if key == nil {
            if doneBlock != nil {
                doneBlock?(nil, nil)
            }
        }
        if doneBlock != nil {
            let json = JSONDataFromMemoryCache(forKey: key)
            doneBlock!(json,nil);
        }
    }
    
    private func JSONDataFromMemoryCache(forKey key: String?) -> Data? {
        let cachedObject = memCache?.getObject(forKey: key! as NSString);
        if cachedObject?.data is Data
        {
            return (cachedObject?.data as! Data)
        }
        return nil;
    }
    
   func store(_ jsonData:Data?, forKey key: String?) {
        if jsonData == nil || key == nil {
            return
        }
        // if memory cache is enabled
        if config.shouldCacheJSONInMemory {
            if let jsonData = jsonData {
                let size = jsonData.count ;
                let cacheObj = CacheObject()
                cacheObj.data = jsonData
                cacheObj.dataSize = size
                cacheObj.key = key!
                NSLog("from CacheManager store jsonData %@",key ?? "")
                memCache?.setObject(cacheObj, forKey: key! as NSString, cost: size)
            }
        }
    }
    
    func clearMemory()
    {
        NSLog("from CacheManager clearMemory")
        self.memCache?.removeAll()
    }
    
    
    func setConfig(config_:cacheConfig)
    {
        self.config = config_
    }
    
    @objc private func backgroundDeleteCache() {
        let UIApplicationClass: AnyClass? = NSClassFromString("UIApplication")
        if UIApplicationClass == nil || !(UIApplicationClass?.responds(to: #selector(getter: UIApplication.shared)) ?? false) {
            return
        }
        let application = UIApplication.shared
        var bgTask: UIBackgroundTaskIdentifier?
        bgTask = application.beginBackgroundTask(expirationHandler: {
            if let task = bgTask {
                application.endBackgroundTask(task)
            }
            bgTask = .invalid
        })
        deleteOldFiles()
        if let bgTask = bgTask {
            application.endBackgroundTask(bgTask)
        }
        bgTask = .invalid
    }
    
    @objc private func deleteOldFiles() {
        NSLog("from CacheManager deleteOldFiles")
        var keysToDelete = [String]();
        let expirationDate = Date(timeIntervalSinceNow: TimeInterval(-config.maxCacheAge))
        var currentCacheSize = 0;
        for cacheObj in self.memCache!.getAllValues() {
            let modificationDate = cacheObj.cachedDateTime
            if modificationDate < expirationDate {
                NSLog("from CacheManager deleteOldFiles modificationDate < expirationDate %@",cacheObj.key ?? "")
                keysToDelete.append(cacheObj.key!)
                continue
            }
            let size = cacheObj.dataSize
            currentCacheSize += size
        }
        
        if(config.maxCacheSize > 0 && currentCacheSize > config.maxCacheSize)
        {
            let desiredCacheSize = config.maxCacheSize / 2
            
            let sortedFiles = self.memCache!.getAllValues().sorted { (CacheObject1, CacheObject2) -> Bool in
                CacheObject1.cachedDateTime < CacheObject2.cachedDateTime
            }
            
            for cacheObj in sortedFiles {
                let totalAllocatedSize = cacheObj.dataSize
                currentCacheSize -= totalAllocatedSize
                NSLog("from CacheManager deleteOldFiles currentCacheSize > config.maxCacheSize %@",cacheObj.key ?? "")
                keysToDelete.append(cacheObj.key!)
                if currentCacheSize < desiredCacheSize {
                    break
                }
            }
        }
        for key in keysToDelete{
            self.memCache?.removeObject(forKey: key as NSString)
        }
    }
}
