//
//  Cache.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/29/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

class Cache<KeyType:AnyObject, ObjectType:AnyObject>: NSCache<AnyObject, AnyObject>{
    var weakCache: NSMapTable<KeyType, ObjectType>
    var weakCacheLock: DispatchSemaphore!
    
    override init() {
        weakCache = NSMapTable(keyOptions: .strongMemory, valueOptions: .strongMemory, capacity: 0)
        weakCacheLock = DispatchSemaphore(value: 1)
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveMemWarning(_:)), name: UIApplication.didReceiveMemoryWarningNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didReceiveMemoryWarningNotification, object: nil)
    }
    
    @objc func didReceiveMemWarning(_ notification: Notification?) {
        super.removeAllObjects()
    }
    
    func setObject(_ obj: ObjectType, forKey key: KeyType, cost g: Int) {
        super.setObject(obj, forKey: key, cost: g)
        // Store weak cache
        weakCacheLock.wait()
        weakCache.setObject(obj, forKey: key)
        weakCacheLock.signal()
    }
    
    func getAllValues() -> [ObjectType]
    {
        return self.weakCache.objectEnumerator()?.allObjects as! [ObjectType]
    }
    
    func getObject(forKey key: KeyType) -> ObjectType? {
        var obj = super.object(forKey: key)
        if obj == nil {
            // Check weak cache
            weakCacheLock.wait()
            obj = weakCache.object(forKey: key)
            weakCacheLock.signal()
            if obj != nil {
                // Sync cache
                var cost = 0
                if (obj is Data) {
                    cost = (obj?.count)!;
                }
                super.setObject(obj!, forKey: key, cost: cost)
            }
        }
        if(obj == nil)
        {
            return nil
        }
        
        return (obj as? ObjectType)
    }
    
    func removeObject(forKey key: KeyType) {
        super.removeObject(forKey: key)
        weakCacheLock.wait()
        weakCache.removeObject(forKey: key)
        weakCacheLock.signal()
    }
    
    func removeAll() {
        
        NSLog("from Cache removeAll")
        super.removeAllObjects()
        weakCacheLock.wait()
        weakCache.removeAllObjects()
        weakCacheLock.signal()
    }
    
    
    
}
