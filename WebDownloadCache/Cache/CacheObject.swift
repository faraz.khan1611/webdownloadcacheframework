//
//  cacheObject.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/29/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

class CacheObject: NSObject {
    var data:Any?
    var cachedDateTime = Date();
    var dataSize:Int = 0;
    var key:String?;
}
