//
//  CacheManagerProtocol.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

protocol CacheManagerProtocol:cacheTypeAlias {
    func queryImageCache(forKey key: String?, done doneBlock: cacheOperationCompletedForImage?)
    func queryJSONCache(forKey key: String?, done doneBlock: cacheOperationCompletedForJSONData?)
    func store(_ image: UIImage?, imageData: Data?, forKey key: String?)
    func store(_ jsonData:Data?, forKey key: String?)
    func clearMemory()
    func setConfig(config_:cacheConfig)
}
