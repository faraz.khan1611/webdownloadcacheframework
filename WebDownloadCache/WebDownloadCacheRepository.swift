//
//  WebDownloadCacheRepository.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

public class cacheConfig: NSObject {
    var maxCacheAge = 60 * 60
    var maxCacheSize = 10 * 1024 * 1024 //10 MB
    var shouldCacheImagesInMemory = true;
    var shouldCacheJSONInMemory = true;
}

public protocol RepoProtocol
{
    typealias imageCompletionBlock = (UIImage?, Error?) -> Void
    typealias jsonArrayCompletionBlock = ([Any]?,Data?, Error?) -> Void
}

class WebDownloadCacheRepository: NSObject,RepoProtocol {
    
    private var cacheManager:CacheManagerProtocol!
    private var downloadManager:DownloaderProtocol!
    private var translator:Translator!
    
    init(cacheMgr:CacheManagerProtocol,downloadMgr:DownloaderProtocol,translator_:Translator) {
        cacheManager = cacheMgr
        downloadManager = downloadMgr
        translator = translator_
        super.init()
    }
    
    init(cacheMgr:CacheManagerProtocol,downloadMgr:DownloaderProtocol,translator_:Translator,config:cacheConfig) {
        cacheManager = cacheMgr
        cacheManager.setConfig(config_: config)
        downloadManager = downloadMgr
        translator = translator_
        super.init()
    }
    
    func loadImage(with url: URL?, fromCache loadFromCache:Bool, completed completedBlock: @escaping imageCompletionBlock) -> WebDownloadCacheRepositoryOperation?{
        weak var weakSelf = self
        let strongDataRepoOperation = WebDownloadCacheRepositoryOperation()
        weak var weakDataRepoOperation = strongDataRepoOperation
        if(loadFromCache)
        {
            self.cacheManager.queryImageCache(forKey: url?.absoluteString) { (image, error) in
                if(error != nil)
                {
                    NSLog("from queryImageCache error url %@",url?.absoluteString ?? "")
                    completedBlock(nil,error)
                }
                else if(image != nil)
                {
                    NSLog("from queryImageCache image url %@",url?.absoluteString ?? "")
                    completedBlock(image,nil)
                }
                else
                {
                    NSLog("from calling downloadData url %@",url?.absoluteString ?? "")
                    weakDataRepoOperation?.token =  weakSelf?.downloadManager.downloadData(with: url, isURLCachingEnabled: true, completed: { (data, error, finished) in
                        if(error != nil)
                        {
                            NSLog("from downloadData error url %@",url?.absoluteString ?? "")
                            completedBlock(nil,error)
                        }
                        else if(data != nil)
                        {
                            NSLog("from downloadData image url %@",url?.absoluteString ?? "")
                            let image = weakSelf?.translator.convertToImage(from: data!)
                            weakSelf?.cacheManager.store(image, imageData: data, forKey: url?.absoluteString)
                            completedBlock(image,nil)
                        }
                    })
                    
                }
            }
        }
        else
        {
            weakDataRepoOperation?.token = downloadManager.downloadData(with: url, isURLCachingEnabled: true, completed: { (data, error, finished) in
                if(error != nil)
                {
                    NSLog("from downloadData error url %@",url?.absoluteString ?? "")
                    completedBlock(nil,error)
                }
                else if(data != nil)
                {
                    NSLog("from downloadData image url %@",url?.absoluteString ?? "")
                    let image = weakSelf?.translator.convertToImage(from: data!)
                    weakSelf?.cacheManager.store(image, imageData: data, forKey: url?.absoluteString)
                    completedBlock(image,nil)
                }
            })
        }
        return strongDataRepoOperation
    }
    
    func loadJSONData(with url: URL?, fromCache loadFromCache:Bool = false, completed completedBlock: @escaping jsonArrayCompletionBlock) -> WebDownloadCacheRepositoryOperation?{
        weak var weakSelf = self
        let strongDataRepoOperation = WebDownloadCacheRepositoryOperation()
        weak var weakDataRepoOperation = strongDataRepoOperation
        if(loadFromCache)
        {
            self.cacheManager.queryJSONCache(forKey: url?.absoluteString, done:{ (jsonData, error) in
                if(error != nil)
                {
                    completedBlock(nil,nil,error)
                }
                else if(jsonData != nil)
                {
                    
                    let jsonArray = weakSelf?.translator.convertToJSONArray(from: jsonData!)
                    if(jsonArray != nil)
                    {
                        completedBlock(jsonArray,jsonData,nil)
                    }
                    else{
                        completedBlock(nil,jsonData,nil)
                    }
                }
                else
                {
                    weakDataRepoOperation?.token =  weakSelf?.downloadManager.downloadData(with: url, isURLCachingEnabled: true, completed: { (data, error, finished) in
                        if(error != nil)
                        {
                            completedBlock(nil,nil,error)
                        }
                        else if(data != nil)
                        {
                            weakSelf?.cacheManager.store(data, forKey: url?.absoluteString)
                            let jsonArray = weakSelf?.translator.convertToJSONArray(from: data!)
                            if(jsonArray != nil)
                            {
                                completedBlock(jsonArray,data,nil)
                            }
                            completedBlock(jsonArray,data,nil)
                        }
                    })
                    
                }
            })
        }
        else
        {
            weakDataRepoOperation?.token =  weakSelf?.downloadManager.downloadData(with: url, isURLCachingEnabled: true, completed: { (data, error, finished) in
                if(error != nil)
                {
                    completedBlock(nil,nil,error)
                }
                else if(data != nil)
                {
                    weakSelf?.cacheManager.store(data, forKey: url?.absoluteString)
                    let jsonArray = weakSelf?.translator.convertToJSONArray(from: data!)
                    if(jsonArray != nil)
                    {
                        completedBlock(jsonArray,data,nil)
                    }
                    else
                    {
                        completedBlock(nil,data,nil)
                    }
                }
            })
        }
        return strongDataRepoOperation
    }
    
    func cancelAllDownloads()
    {
        self.downloadManager.cancelAllDownloads()
    }
}
