//
//  WebDownloadCacheSDK.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

public class WebDownloadCacheSDK: NSObject,RepoProtocol {

    private static let shared = WebDownloadCacheSDK()
    
    private var cacheProtocol:CacheManagerProtocol!
    private var downloaderProtocol:DownloaderProtocol!
    private var translator:Translator!
    private var dataRepo:WebDownloadCacheRepository!
    
    private override init()
    {
        cacheProtocol = CacheManager()
        downloaderProtocol = Downloader()
        translator = Translator()
        dataRepo = WebDownloadCacheRepository(cacheMgr: cacheProtocol, downloadMgr: downloaderProtocol, translator_: translator)
        super.init()
    }
    
    static public func getSharedSDK(cacheConfig:cacheConfig? = nil) -> WebDownloadCacheSDK
    {
        if((cacheConfig) == nil)
        {
            return shared
        }
        else
        {
            shared.cacheProtocol.setConfig(config_: cacheConfig!)
            return shared
        }
    }
    
    public func loadImage(with url: URL?, fromCache loadFromCache:Bool, completed completedBlock: @escaping imageCompletionBlock) -> WebDownloadCacheRepositoryOperation?
    {
        return self.dataRepo.loadImage(with: url, fromCache: loadFromCache, completed: completedBlock)
    }
    
    public func loadJSONData(with url: URL?, fromCache loadFromCache:Bool = false, completed completedBlock: @escaping jsonArrayCompletionBlock) -> WebDownloadCacheRepositoryOperation?
    {
        return self.dataRepo.loadJSONData(with: url,fromCache:loadFromCache, completed: completedBlock)
    }
}
