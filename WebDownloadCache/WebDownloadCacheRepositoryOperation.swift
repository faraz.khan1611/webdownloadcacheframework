//
//  WebDownloadCacheRepositoryOperation.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

public class WebDownloadCacheRepositoryOperation: NSObject {
    
    var token:DownloadToken?
    var downloader:DownloaderProtocol?
    
    func cancel()
    {
        let lockQueue = DispatchQueue(label: "self")
        lockQueue.sync {
            downloader?.cancel(token)
        }
    }
}

