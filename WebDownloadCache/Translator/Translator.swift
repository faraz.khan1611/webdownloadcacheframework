//
//  Translator.swift
//  WebDownloadCache
//
//  Created by Faraz Khan on 6/30/19.
//  Copyright © 2019 Faraz Khan. All rights reserved.
//

import UIKit

class Translator: NSObject {
    
    func convertToImage(from data:Data) -> UIImage?
    {
        return UIImage(data: data)
    }
    
    func convertToJSONArray(from data:Data) -> [Any]?
    {
        let jsonArray = try? JSONSerialization.jsonObject(with: data, options: []) as? [Any]
        return jsonArray ?? nil
    }
    
    func convertToJSONObject(from data:Data) -> [String:Any]?
    {
        let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? [String:Any]
        return jsonObject ?? nil
    }
}
